function outerFunction() {
    const message = “Hello closure”
   function innerFunction() {
        console.log(message);
   }
return innerFunction();
}
const closure = outerFunction(); 
closure(); // Prints out “Hello closure”