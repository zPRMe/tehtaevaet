function car (name, engine, wheels, year) { // constructor function
    this.name = name;
    this.engine = engine;
    this.wheels = wheels;
    this.year = year;
    this.hasTrunk = true;
    this.velocity = 50;
    this.brake = function() {this.velocity = 0;};
 }
 
 car();