const messages = " Hyvää päivää maailmanmaha ";

console.log(messages.trim());

// NEXT

let message1 = "Hyvää päivää maailman aamupäivän aamu";

console.log(message1.substring(20));

// NEXT

let message = "Hyvää päivää maailman aamupäivän aamu";

console.log(message.toLowerCase());

// NEXT

let messa = "Hyvää päivää maailman aamupäivän aamu";

console.log(messa.toUpperCase());

// TESTS

let mes = "Hienoa komea Päivä";

console.log(mes.charAt(3);

